
// Not Mine...

package applications;

public class SynchTest extends Thread {

    // ===========================================
    // Public Members
    // ===========================================

    // ===========================================
    // Private Members
    // ===========================================

    private static final Object LOCK = new Object();

    // ===========================================
    // Static initialisers
    // ===========================================

    // ===========================================
    // Constructors
    // ===========================================

    public SynchTest() {
    }

    // ===========================================
    // Public Methods
    // ===========================================

    public static void main(String[] args) {
        Thread t1 = new Thread(new SynchTest());
        Thread t2 = new Thread(new SynchTest());

        System.out.println(t1.getId());
        System.out.println(t2.getId());

        t1.start();
        t2.start();
    }

    public void writeMessage() {
        synchronized (LOCK) {
            for(int a = 0; a < 2100000000; a++){}
            System.out.println("In write message" + getId());
        }
    }

    private void setMessage() {
        synchronized (LOCK) {
            for(int a = 0; a < 2100000000; a++){}
            System.out.println("In set message" + getId());
        }
    }

    @Override
    public void run() {
        setMessage();
        writeMessage();
    }

    // ===========================================
    // Protected Methods
    // ===========================================

    // ===========================================
    // Private Methods
    // ===========================================

}
