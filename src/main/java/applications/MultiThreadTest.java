
package applications;

import java.lang.Thread.State;

import threads.MyThread;

public class MultiThreadTest {

    public MultiThreadTest() {
    }

    public static void main(String[] args) {
        int i = 0;

        Thread thread1 = new Thread(new MyThread("thread1", 5));
        Thread thread2 = new Thread(new MyThread("---thread2", 5));

        thread1.start();
        thread2.start();

        while (thread1.isAlive()) {
            i++;
            continue;
        }
        System.out.println("Here - " + i);

        if (thread1.getState() == State.TERMINATED) {
            thread1.start();
        }
    }
}
