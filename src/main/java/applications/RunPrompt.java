
package applications;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RunPrompt {

    public static void main(String[] args) {

        List<String> commands = new ArrayList<String>();
        commands.add("cmd.exe");
        commands.add("/C");
        commands.add("start");
        commands.add("cls");

        ProcessBuilder processBuilder = new ProcessBuilder(commands);
        try {
            processBuilder.start();
            // Also
            // Runtime.getRuntime().exec("cmd.exe /C start");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
