
package threads;


public class MyThread extends Thread {// implements Runnable {

    public String str;
    public int times = 500000;

    public MyThread(String str, int times) {
        this.str = str;
        this.times = times;
    }

    @Override
    public void run() {
        loop(this.str, this.times);
    }

    public static void loop(String str, int x) {
        for (int i = 0; i <= x; i++) {
            System.out.println(i + ": " + str);
        }
    }
}
