package threads;

public class SimpleThread extends Thread {
    private final int times;
    private final String lineBreak = "****************";

    public SimpleThread(String string) {
        super(string);
        this.times = 100;
    }

    public SimpleThread(String string, int times) {
        super(string);
        this.times = times;
    }

    @Override
    public void run() {
        for (int i = 0; i < this.times; i++) {
            System.out.println(i + " " + getName());
//            try {
//                sleep((int) (Math.random() * 1000));
//            } catch (InterruptedException e) {
//                return;
//            }
        }
        System.out.println(this.lineBreak);
        System.out.println("FINISHED: " + getName() + "!");
        System.out.println(this.lineBreak);
    }
}
